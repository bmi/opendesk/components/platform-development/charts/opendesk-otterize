## [2.1.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v2.1.2...v2.1.3) (2024-12-20)


### Bug Fixes

* **opendesk-otterize:** Add ClientIntents for notes ([41c6c3b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/41c6c3ba3eff81cbbf652483806731c42f4044d4))

## [2.1.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v2.1.1...v2.1.2) (2024-12-19)


### Bug Fixes

* **opendesk-otterize:** Update policies for openDesk v1.1 ([1237666](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/1237666ce7f19feff473ce25ea3b911a0a708571))

## [2.1.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v2.1.0...v2.1.1) (2024-08-28)


### Bug Fixes

* **opendesk-otterize:** Add network policies for monitoring OX Appsuite ([b66f9af](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/b66f9af94c4237ed50944c9671ed0639cf56e99c))

# [2.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v2.0.1...v2.1.0) (2024-06-28)


### Features

* Add ClientIntents and ProtectedServices to support scalable, rendundant Nubus LDAP server deployments. ([d21f393](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/d21f3933f22fa88445ff8c12ae35a49f12866dec))

## [2.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v2.0.0...v2.0.1) (2024-04-09)


### Bug Fixes

* **opendesk-otterize:** Remove istio ingess for open-xchange appsuite ([a5b1fa1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/a5b1fa1ce701f7288b39914f737056ad69da5639))

# [2.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.8.0...v2.0.0) (2024-04-07)


### Features

* **opendesk-otterize:** Remove NetworkPolicies for Ingress ([6b0a874](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/6b0a874e47a057be666960a8c7d5d400e722bedc))


### BREAKING CHANGES

* **opendesk-otterize:** Remove Ingress NetworkPolicies which are not needed with Otterize Intentens Operator v1.27

# [1.8.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.7.9...v1.8.0) (2024-04-07)


### Bug Fixes

* **opendesk-otterize:** ldap-server needs no access to postgresql ([bd7d010](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/bd7d0104dca6c4cfcb11e5d83aec3073d0fc53c4))


### Features

* **opendesk-otterize:** accommodate for Nubus umbrella chart ([441fa4e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/441fa4e55eaf130217aa1ac18309c6fd19bc9894))

## [1.7.9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.7.8...v1.7.9) (2024-03-25)


### Bug Fixes

* **opendesk-otterize:** Allow OpenProject Seedter to call ldap-server ([592d975](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/592d9753df7100c16c1b087419d06e99dcadd229))

## [1.7.8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.7.7...v1.7.8) (2024-03-25)


### Bug Fixes

* **opendesk-otterize:** Upate podSelector for Nextcloud exporter ([f612ee3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/f612ee34f5bbb04f6250aa442c9f8e1df00714d0))

## [1.7.7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.7.6...v1.7.7) (2024-03-25)


### Bug Fixes

* **opendesk-otterize:** Add Nextcloud Exporter ([5978d6b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/5978d6b78a463069a9822c320315117ad2f23542))

## [1.7.6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.7.5...v1.7.6) (2024-03-21)


### Bug Fixes

* **univention-provisioning:** add nats to the udm-listener connections ([1e29529](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/1e29529be528ae9c0e4c4baa57f3230647cc379a))

## [1.7.5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.7.4...v1.7.5) (2024-02-28)


### Bug Fixes

* **univention-provisioning:** udm-listener intent to provisioing-api ([c72c32b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/c72c32b60fc216168f2c20c4cbbd777d606fa2bb))

## [1.7.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.7.3...v1.7.4) (2024-02-27)


### Bug Fixes

* **univention-provisioning:** Declare provisioning intents and protected services ([657dbf1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/657dbf1b3de56613eee262144aba05ee23a0d2f6))

## [1.7.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.7.2...v1.7.3) (2024-02-12)


### Bug Fixes

* **univention-portal:** drop store-dav in favor of object-storage ([888631a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/888631a561d7026e1bb81fe5dba865bc53cbf04b))

## [1.7.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.7.1...v1.7.2) (2024-01-31)


### Bug Fixes

* **ums-provisioning:** Fix filename ([f531dda](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/f531ddabe611972e6ef76fac350a3666fc594c51))

## [1.7.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.7.0...v1.7.1) (2024-01-29)


### Bug Fixes

* **opendesk-otterize:** Allow connection between umc-server and memcached ([bf594b0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/bf594b067d43651c28fc83297ef430ef2e9529d0))

# [1.7.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.6.0...v1.7.0) (2024-01-17)


### Features

* **opendesk-otterize:** UMC server policies to keycloak and postfix ([3747a4a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/3747a4a7b8ce288e6e278d14bb0032e2495c28e8))

# [1.6.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.5.0...v1.6.0) (2024-01-06)


### Features

* Add intent rules for ClamAV ICAP access to OX core-mw ([ca20008](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/ca200081cb0994870b275aec38d0eed1a90faaad))

# [1.5.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.4.0...v1.5.0) (2024-01-02)


### Features

* **opendesk-otterize:** Add rules for Guardian components ([25a3b94](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/25a3b943f83bb82c5bfb0573a22095d87d4e63ab))

# [1.4.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.3.2...v1.4.0) (2024-01-02)


### Bug Fixes

* **opendesk-otterize:** Fix address of nextcloud in open-xchange-core-mw-default ([9deab35](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/9deab35656907b6e47583596637cfbe927e58d1f))


### Features

* **opendesk-otterize:** Replace nextcloud by opendesk-nextcloud ([a598309](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/a598309f4aa33b30dc8c1ed608b5db5fef35a8d4))

## [1.3.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.3.1...v1.3.2) (2023-12-21)


### Bug Fixes

* Add GPG key, update README.md ([6969a11](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/6969a117a4cfd80663b22862795197891e4e5cd9))

## [1.3.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/compare/v1.3.0...v1.3.1) (2023-12-20)


### Bug Fixes

* **ci:** Move repo to Open CoDE ([a0e98da](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize/commit/a0e98da2179fb6ad190f1b3cb25f111a587870c5))

# [1.3.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.2.0...v1.3.0) (2023-12-20)


### Features

* **otterize:** add rules for UMS provisioning ([141a57a](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/141a57ad5c65a5a3d9362372ae2d4abc1e074943))

# [1.2.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.1.6...v1.2.0) (2023-12-15)


### Features

* Add Univention Keycloak ([bba0507](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/bba05073cb3d51fdfa087f6a79ade38e81a8d8f9))

## [1.1.6](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.1.5...v1.1.6) (2023-12-13)


### Bug Fixes

* **opendesk-otterize:** Remove Univention Corporate Container ([f24fa41](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/f24fa4112df2da1940f9d063304085c66786b7c9))

## [1.1.5](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.1.4...v1.1.5) (2023-12-12)


### Bug Fixes

* **opendesk-otterize:** Add UMS Selfservice Listener to UMC server ([9b50274](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/9b502746a5ae1c6dc5d86aa577c3c9744d4eca9f))

## [1.1.4](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.1.3...v1.1.4) (2023-12-11)


### Bug Fixes

* **opendesk-otterize:** Add configuration for UMS Selfservice Listener ([b1e56e3](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/b1e56e35b0027802ba34ffcac0dcf899c00a7754))

## [1.1.3](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.1.2...v1.1.3) (2023-12-05)


### Bug Fixes

* **opendesk-otterize:** Add configuration for UMS Portal Server ([6c172e1](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/6c172e1a0dffd64e226e335222cd3531fbe3f0d0))
* **opendesk-otterize:** Add configuration for UMS Stack Gateway ([9776dcb](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/9776dcbe4a4c9e236f3f42a3b1748658af3c80db))
* **opendesk-otterize:** Allow UMS UMC Server to reach PostgreSQL ([b9a52bd](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/b9a52bd9bd0a6f98aecb2bede34438d7e921dfe3))

## [1.1.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.1.1...v1.1.2) (2023-11-30)


### Bug Fixes

* **opendesk-otterize:** Update comoponents for updated UMS ([194b90f](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/194b90fdc31a6a32f73091de00f99a980da223de))

## [1.1.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.1.0...v1.1.1) (2023-11-28)


### Bug Fixes

* **opendesk-otterize:** Add missing protection for minio, redis and nextcloud-metrics ([16dfa47](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/16dfa471eac1e69380406db026f6fec29c06ae5b))

# [1.1.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.0.5...v1.1.0) (2023-11-27)


### Features

* **opendesk-otterize:** Add NetworkPolicies to allow IngressController and Istio ([080fca1](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/080fca11ccdaf617b70168dbf6854df13a4139c4))

## [1.0.5](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.0.4...v1.0.5) (2023-11-27)


### Bug Fixes

* **opendesk-otterize:** Replace ClientIntent for external postfix with standard network policy ([2476942](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/2476942f09ecfb0f2b5e5fea8d68b2cf68883e27))

## [1.0.4](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.0.3...v1.0.4) (2023-11-26)


### Bug Fixes

* **opendesk-otterize:** Add LDAP server connection for ums-umc-server ([e1140d8](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/e1140d81af421dc68c2ab377629f7cb9150f635b))

## [1.0.3](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.0.2...v1.0.3) (2023-11-26)


### Bug Fixes

* **opendesk-otterize:** Add ums-ldap-notifier connection to ox-connector ([3abd3fc](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/3abd3fc280db825510e6b9ebec2730423624d89b))

## [1.0.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.0.1...v1.0.2) (2023-11-26)


### Bug Fixes

* **opendesk-otterize:** Fix UMS LDAP host ([246d028](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/246d028d3c8671a94fe3070c14c29c75f9e869ab))

## [1.0.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/compare/v1.0.0...v1.0.1) (2023-11-26)


### Bug Fixes

* **opendesk-otterize:** Add ums to all ucs calls intents ([3db8620](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/3db862020ba1e2284bf4ac22aa15097691d519ba))

# 1.0.0 (2023-11-26)


### Features

* **opendesk-otterize:** Add opendesk-otterize chart ([22d98d2](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-otterize/commit/22d98d29cea551d85a7729f1750fbfd0e40df84a))
