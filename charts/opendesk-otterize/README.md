<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# opendesk-otterize

A Helm chart deploying resources for Otterize to secure services with NetworkPolicies.

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository

```console
helm repo add opendesk-otterize https://gitlab.opencode.de/api/v4/projects/2293/packages/helm/stable
helm install my-release --version 2.1.3 opendesk-otterize/opendesk-otterize
```

### Install via OCI Registry

```console
helm repo add opendesk-otterize oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize
helm install my-release --version 2.1.3 opendesk-otterize/opendesk-otterize
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry.opencode.de/bmi/opendesk/components/external/charts/bitnami-charts | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| additionalAnnotations | object | `{}` | Additional custom annotations to add to all deployed objects. |
| additionalLabels | object | `{}` | Additional custom labels to add to all deployed objects. |
| apps.clamavDistributed.enabled | bool | `true` | Enables ClamAV (in distributed mode) related resource creation. |
| apps.clamavDistributed.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.clamavDistributed.signatureHost | string | `"gitlab.opencode.de"` | Signature database host |
| apps.clamavSimple.enabled | bool | `true` | Enables ClamAV (in simple mode) related resource creation. |
| apps.clamavSimple.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.clamavSimple.signatureHost | string | `"gitlab.opencode.de"` | Signature database host |
| apps.collabora.enabled | bool | `true` | Enables Collabora related resource creation. |
| apps.collabora.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.cryptpad.enabled | bool | `true` | Enables Cryptpad related resource creation. |
| apps.cryptpad.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.dkimpy.enabled | bool | `true` | Enables dkimpy related resource creation. |
| apps.dkimpy.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.dovecot.enabled | bool | `true` | Enables Dovecot related resource creation. |
| apps.dovecot.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.element.enabled | bool | `true` | Enables Element related resource creation. |
| apps.element.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.intercom.enabled | bool | `true` | Enables Intercom Service related resource creation. |
| apps.intercom.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.jitsi.enabled | bool | `true` | Enables Jitsi related resource creation. |
| apps.jitsi.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.mariadb.enabled | bool | `true` | Enables MariaDB related resource creation. |
| apps.mariadb.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.memcached.enabled | bool | `true` | Enables Memcached related resource creation. |
| apps.memcached.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.minio.enabled | bool | `true` | Enables MinIO related resource creation. |
| apps.minio.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.nextcloud.enabled | bool | `true` | Enables Nextcloud related resource creation. |
| apps.nextcloud.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.notes.enabled | bool | `true` | Enables LaSuite Notes related resource creation. |
| apps.notes.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.nubus.enabled | bool | `true` | Enables Univention Management Stack related resource creation. |
| apps.nubus.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.openproject.enabled | bool | `true` | Enables OpenProject related resource creation. |
| apps.openproject.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.oxAppsuite.enabled | bool | `true` | Enables Open-Xchange Appsuite related resource creation. |
| apps.oxAppsuite.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.oxConnector.enabled | bool | `true` | Enables OX-Connector related resource creation. |
| apps.oxConnector.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.postfix.enabled | bool | `true` | Enables Postfix related resource creation. |
| apps.postfix.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.postgresql.enabled | bool | `true` | Enables PostgreSQL related resource creation. |
| apps.postgresql.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.redis.enabled | bool | `true` | Enables Redis related resource creation. |
| apps.redis.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| apps.xwiki.enabled | bool | `true` | Enables XWiki related resource creation. |
| apps.xwiki.namespace | string | `""` | If omitted, resources are deployed in the same namespace as this helm chart. |
| clientIntents.apiVersion | string | `"k8s.otterize.com/v1alpha3"` | Choose the API version to use. |
| clientIntents.enabled | bool | `true` | Enable creation of ClientIntents custom resource. |
| extraApps.clusterPostfix.enabled | bool | `false` | Enables cluster-wide postfix related resource creation. |
| extraApps.clusterPostfix.namespace | string | `"swp-cross-instance-mail"` | If omitted, resources are deployed in the same namespace as this helm chart. |
| global.domain | string | `"example.internal"` | Deployment base domain used for egress restrictions to opendesk services via Ingress. |
| global.hosts | object | `{"collabora":"office","cryptpad":"pad","element":"chat","intercomService":"ics","jitsi":"meet","keycloak":"id","matrixNeoBoardWidget":"matrix-neoboard-widget","matrixNeoChoiceWidget":"matrix-neochoice-widget","matrixNeoDateFixBot":"matrix-neodatefix-bot","matrixNeoDateFixWidget":"matrix-neodatefix-widget","minioApi":"objectstore","minioConsole":"objectstore-ui","nextcloud":"files","notes":"notes","nubus":"portal","openproject":"projects","openxchange":"webmail","synapse":"matrix","synapseFederation":"matrix-federation","whiteboard":"whiteboard","xwiki":"wiki"}` | A map of avaible deployment subdomains. |
| ingressController.namespace | string | `"nginx-ingress"` | Namespace of ingress controller. |
| ingressController.podSelector | object | `{"matchLabels":{"app.kubernetes.io/name":"nginx-ingress"}}` | Pod selector for ingress controller to match for NetworkPolicies. |
| istioGateway.namespace | string | `"istio-system"` | Namespace of ingress controller. |
| istioGateway.podSelector | object | `{"matchLabels":{"app":"gateway","istio":"gateway"}}` | Pod selector for ingress controller to match for NetworkPolicies. |
| networkPolicies.enabled | bool | `true` | Enable creation of NetworkPolicies custom resource. |
| prometheus.namespace | string | `"monitoring"` | Namespace of ingress controller. |
| prometheus.podSelector | object | `{"matchLabels":{"app.kubernetes.io/name":"prometheus"}}` | Pod selector for ingress controller to match for NetworkPolicies. |
| protectedServices.apiVersion | string | `"k8s.otterize.com/v1alpha3"` | Choose the API version to use. |
| protectedServices.enabled | bool | `true` | Enable creation of ProtectedServices custom resource. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
