<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# openDesk Otterize

This repository contains a chart to deploy intent-based access control (IBAC) for openDesk services based on Otterize
OSS intent files.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Kubernetes 1.21+
- Helm 3.0.0+
- [Otterize intents operator](https://github.com/otterize/intents-operator)

## Documentation

The documentation is placed in the README of each helm chart:

- [opendesk-otterize](charts/opendesk-otterize)

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
